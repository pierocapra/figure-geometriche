package corso.lez6.disegnotecnico_2;	

public class Main {

	public static void main(String[] args) {
		
				//DICHIARAZIONE FIGURE
				Triangolo tri_1 = new Triangolo(3.5f, 6.8f);	
				Quadrato quad_1 = new Quadrato(3.5f);
				Cerchio cerc_1 = new Cerchio(2f);

				//STAMPE E LOGICA
				System.out.println(tri_1);
				System.out.println(quad_1);
				System.out.println(cerc_1);
				
				System.out.println("Area triangolo: "+ tri_1.CalcoloArea());
				System.out.println("Area quadrato: "+ quad_1.CalcoloArea());
				System.out.println("Area cerchio: "+ cerc_1.CalcoloArea());

	}

}

package corso.lez6.disegnotecnico_2;

public class Quadrato implements CalcoliArea{
	
	private double lato;

	// CONSTRUCTORS
	public Quadrato() {}
	
	public Quadrato(double lato) {
		this.lato = lato;
	}

	//GETTERS AND SETTERS
	public double getLato() {
		return lato;
	}

	public void setLato(double lato) {
		this.lato = lato;
	}

	//TOSTRING
	@Override
	public String toString() {
		return "Quadrato [lato=" + lato + "]";
	}
	
	//METODI INTERNI
	public double CalcoloArea() {
		double area = lato*lato;
		return area;
	};
	
}

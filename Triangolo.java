package corso.lez6.disegnotecnico_2;

public class Triangolo implements CalcoliArea{

	private double base;
	private double altezza;
	
	//CONSTRUCTORS
	
	public Triangolo() {}
	
	public Triangolo(double base, double altezza) {
		this.base = base;
		this.altezza = altezza;
	}

	//GETTERS AND SETTERS
	public double getBase() {
		return base;
	}

	public void setBase(double base) {
		this.base = base;
	}

	public double getAltezza() {
		return altezza;
	}

	public void setAltezza(double altezza) {
		this.altezza = altezza;
	}

	//TOSTRING
	@Override
	public String toString() {
		return "Triangolo [base=" + base + ", altezza=" + altezza + "]";
	}
	
	//METODI INTERNI
	public double CalcoloArea() {
		double area = (this.base * this.altezza)/2;
		return area;
	};
}

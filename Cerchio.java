package corso.lez6.disegnotecnico_2;

public class Cerchio implements CalcoliArea{
	
	private double raggio;
	

	//CONSTRUCTOR 
	public Cerchio() {
		
	}

	public Cerchio(double raggio) {
		this.raggio = raggio;
	}

	//GETTERS AND SETTERS
	public double getRaggio() {
		return raggio;
	}

	public void setRaggio(double raggio) {
		this.raggio = raggio;
	}

	//TOSTRING
	@Override
	public String toString() {
		return "Cerchio [raggio=" + raggio + "]";
	}

	//METODI INTERNI
	public double CalcoloArea() {
		double area =Math.PI * Math.pow(raggio, 2);
		return area;
	};
}
